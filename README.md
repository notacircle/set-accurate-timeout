

#### Sets an accurate timeout that respects date, and the result can't be less than the specified amount of milliseconds. Implemented by recursive execution of native setTimeout. The error is about 10ms.

#### Install package

```bash
npm install set-accurate-timeout
```

#### Install dependencies and build library

- Install dependencies

  `npm install`

- Run tests

  `npm run test`

#### Usage example

```js
const setAccurateTimeout=require('set-accurate-timeout')
setAccurateTimeout(() => {
  console.log('So, 5s have passed.')
}, 5000)
```

