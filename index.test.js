const setAccurateTimeout=require('./')

const DELAY_MAX=4096
const delays=[]
for(let delay=2;
		delay<=DELAY_MAX;
		delay*=2) delays.push(delay)

describe.each(delays)('guaranteed', (delay)=>{
	test(`${delay}ms timeout is guaranteed`, done=>{
		const start=new Date().valueOf()
		setAccurateTimeout(()=>{
			const now=new Date().valueOf()
			const diff=now-start
			expect(diff).toBeGreaterThan(0)
			done()
		}, delay)
	}, DELAY_MAX+1000)
})


describe.each(delays)('error', (delay)=>{
	test(`${delay}ms timeout error is less than 10ms`, done=>{
		const start=new Date().valueOf()
		setAccurateTimeout(()=>{
			const now=new Date().valueOf()
			const error=now-start-delay
			expect(Math.abs(error)).toBeLessThan(10)
			done()
		}, delay)
	}, DELAY_MAX+1000)
})
