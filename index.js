const debug = require('debug')('set-accurate-timeout')

module.exports = (callback, delay) => {
	const start = new Date().valueOf()
	setTimeout(function adjust() {
		const now = new Date().valueOf()
		const passed = now-start
		const lacking = delay-passed
		if(passed<delay) {
			debug(`Lacking: ${lacking}.`)
			setTimeout(adjust, lacking)
		} else callback()
		// To run more precisely we need to
		// make more than one iteration. So
		// it's necessary to sub some value
		// from the first iteration.
	}, delay-5)
}
